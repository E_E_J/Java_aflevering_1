
import java.util.Random;
import java.awt.Point;

public class CopRobberII {
	
	public static Random terning = new Random();
	public static Point cop = new Point();
	public static Point robber = new Point();
	
	public static void main(String[] args) {
		CopRobberII.runRandomSimulation(10, 30);
		CopRobberII.runStrategicSimulation(15, 50);
	}
	
	public static void runRandomSimulation(int n, int t) {
	
		System.out.println("n=" + n + " t=" + t);
		
		if ((n <= 0) || (t < 0)) {
			System.out.println("Illegal Parameters!");
		} else {
			
			cop.setLocation(terning.nextInt(n), terning.nextInt(n));
			robber.setLocation(terning.nextInt(n), terning.nextInt(n));
		
			System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]");
			
			int count = 0;
			while (!robber.equals(cop) && count < t) {
				
				moveRobberRandom();
				
				testPos(n);
				
				moveCopRandom();
				
				System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]");
				count++;
			}
			
			if (robber.equals(cop)) {
			System.out.println("Catch!");
			}
			
		}
	}
	
	public static void runStrategicSimulation(int n, int t) {
		
		System.out.println("n=" + n + " t=" + t);
		
		if ((n <= 0) || (t < 0)) {
			System.out.println("Illegal Parameters!");
		} else {
			
			placement(n);
				
			testPos(n);
			
			System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]");
			
			int count = 0;
			while (!robber.equals(cop) && count < t) {
				
				moveRobber(n);
				
				testPos(n);
				
				moveCopRandom();
				
				System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]");
				count++;
			}
			
			if (robber.equals(cop)) {
			System.out.println("Catch!");
			}
			
		}
	}
	
	public static void moveRobberRandom() {
		if (terning.nextInt(2) == 0) {
			if (terning.nextInt(2) == 0) {
				robber.translate(1, 0);
			} else {
				robber.translate(-1, 0);
			}
		} else {
			if (terning.nextInt(2) == 0) {
				robber.translate(0, 1);
			} else {
				robber.translate(0, -1);
			}
		}
	}
	
	public static void moveRobber(int n) {
		if (robber.x == cop.x) {
			if (cop.x > (n/2-1)) {
				robber.x--;
			} else {
				robber.x++;
			}
		} else {
			if (cop.y > (n/2-1)) {
				robber.y--;
			} else {
				robber.y++;
			}
		}
	}
	
	public static void testPos(int n) {
		if (robber.x < 0) robber.x = 0;
		if (robber.x > n) robber.x = n;
		if (robber.y < 0) robber.y = 0;
		if (robber.y > n) robber.y = n;
	}
	
	public static void moveCopRandom() {
		
		int x = robber.x - cop.x;
		int y = robber.y - cop.y;
		
		if (Math.abs(x) > Math.abs(y)) {
			if (cop.x > robber.x) {
				--cop.x;
			} else if (cop.x < robber.x) {
				++cop.x;
			}
		} else {
			if (cop.y > robber.y) {
				--cop.y;
			} else if (cop.y < robber.y) {
				++cop.y;
			}
		}
	}
	
	public static void placement(int n) {
		
		cop.setLocation(terning.nextInt(n), terning.nextInt(n));
		
		if (cop.x > (n/2-1)) {
			robber.setLocation(cop.x-1, cop.y);
		} else {
			robber.setLocation(cop.x+1, cop.y);
		}
		
	}
}

	
