
import java.util.Random;
import java.awt.Point;

public class CopRobber {
	public static void main(String[] args) {
		runSimulation(40,3,100);
	}
	
	public static void runSimulation(int n, int s, int t) {
	
		System.out.println("n=" + n + " s=" + s + " t=" + t);
		
		if ((n <= 0) || (s <= 0) || (t < 0)) {
			System.out.println("Illegal Parameters!");
		} else {
			
			Random terning = new Random();
			Point robber = new Point(terning.nextInt(n), terning.nextInt(n));
			Point cop = new Point(terning.nextInt(n), terning.nextInt(n));
		
			System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]");
			
			int count = 0;
			while (!robber.equals(cop) && count < t) {
				
				robber.translate((terning.nextInt(2 * s + 1) - s), (terning.nextInt(2 * s + 1) - s));
				
				if (robber.x < 0) robber.x = 0;
				if (robber.x > n) robber.x = n;
				if (robber.y < 0) robber.y = 0;
				if (robber.y > n) robber.y = n;
				count++;
				
				for (int i = 0; i < s; ++i) {
					if (cop.x > robber.x) {
						--cop.x;
					} else if (cop.x < robber.x) {
						++cop.x;
					}
				}
				
				for (int i = 0; i < s; ++i) {
					if (cop.y > robber.y) {
						--cop.y;
					} else if (cop.y < robber.y) {
						++cop.y;
					}
				}
				
				
				System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]");
				
			}
			
			if (robber.equals(cop)) {
			System.out.println("Catch!");
			}
		}
	}
}