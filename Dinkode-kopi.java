
import java.util.*;

public class Dinkode {
	public static void main(String[] args) {
		Dinkode();
	}
	
	public static void Dinkode() {
		
		Scanner console = new Scanner(System.in);
		String input = console.nextLine();
		boolean hasDigit = false;
		boolean hasUppercase = false;
		boolean hasLowercase = false;
		boolean hasSymbol = false;
		boolean hasLength = false;
		char c;
		
		//Her siges der at længden på inputtet skal være større end 7
		if (input.length() >= 7) {
			hasLength = true;
		}
		//Her køre vi et for loop igennem længden af inputtet	
		for (int i = 0; i < input.length(); i++) {
			//Her deklarer vi en character på i's plads i loopet
			c = input.charAt(i);
			
			//Her ser vi om inputtet indeholder egenskaberne fra opgaven
			if (Character.isUpperCase(c)) {
				hasUppercase = true;
			} else if (Character.isLowerCase(c)) {
				hasLowercase = true;
			} else if (Character.isDigit(c)) {
				hasDigit = true;
			} else {
				hasSymbol = true; //De første tre giver god mening da den ser om inputtet indeholder et tal og et stort
								  //eller et lille bogstav. Der ses at der er et symbol hvis ingen af de tre kriterier før er mødt
			}
			
		}
		//Her bestemmer vi om inputtet indeholder det den skal og giver et feedback på inputtet. 
		if (hasLength && hasDigit && hasUppercase && hasLowercase && hasSymbol) {
			System.out.print("true");
		} else {
			System.out.print("false");
		}
	}
}
