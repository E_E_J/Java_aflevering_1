
import java.awt.Point;
import java.util.Random;

public class CopCopRobber {
	
	public static Random terning = new Random();
	public static Point cop = new Point();
	public static Point cop2 = new Point();
	public static Point robber = new Point();
	
	public static void main(String[] args) {
		CopCopRobber.runTwoCopsRandom(20, 30);
		CopCopRobber.runTwoCops(25, 30);
	}
	
	public static void runTwoCopsRandom(int n, int t) {
		
		System.out.println("n=" + n + " t=" + t);
		
		if ((n <= 0) || (t < 0)) {
			System.out.println("Illegal Parameters!");
		} else {
			
			placement(n);
			
			testPos(n);
		
			System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]" + "[" + cop2.x + ";" + cop2.y + "]");
			
			int count = 0;
			while (!robber.equals(cop) && !robber.equals(cop2) && count < t) {
				
				moveRobberRandom();
				
				testPos(n);
				
				moveCopRandom(cop);
				
				moveCopRandom(cop2);
				
				System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]" + "[" + cop2.x + ";" + cop2.y + "]");
				count++;
			}
			
			if (robber.equals(cop) || robber.equals(cop2)) {
			System.out.println("Catch!");
			}
			
		}
	}
	
	public static void runTwoCops(int n, int t) {
		
		System.out.println("n=" + n + " t=" + t);
		
		if ((n <= 0) || (t < 0)) {
			System.out.println("Illegal Parameters!");
		} else {
			
			placement(n);
				
			testPos(n);
			
			System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]" + "[" + cop2.x + ";" + cop2.y + "]");
			
			int count = 0;
			while (!robber.equals(cop) && !robber.equals(cop2) && count < t) {
				
				moveRobber(n);
				
				testPos(n);
				
				moveCopRandom(cop);
				
				moveCopRandom(cop2);
				
				System.out.println("[" + robber.x + ";" + robber.y + "]" + "[" + cop.x + ";" + cop.y + "]" + "[" + cop2.x + ";" + cop2.y + "]");
				count++;
			}
			
			if (robber.equals(cop) || robber.equals(cop2)) {
			System.out.println("Catch!");
			}
			
		}
	}
	
	public static void placement(int n) {
		
		cop.setLocation(terning.nextInt(n), terning.nextInt(n));
		cop2.setLocation(n-cop.x, n-cop.y);
		
		if (cop.x > (n/2-1)) {
			robber.setLocation(cop.x-1, cop.y);
		} else {
			robber.setLocation(cop.x+1, cop.y);
		}
		
	}
	
	public static void testPos(int n) {
		if (robber.x < 0) robber.x = 0;
		if (robber.x > n) robber.x = n;
		if (robber.y < 0) robber.y = 0;
		if (robber.y > n) robber.y = n;
	}
	
	public static void moveCopRandom(Point chosen) {
		
		int x = robber.x - chosen.x;
		int y = robber.y - chosen.y;
		
		if (Math.abs(x) > Math.abs(y)) {
			if (chosen.x > robber.x) {
				--chosen.x;
			} else if (chosen.x < robber.x) {
				++chosen.x;
			}
		} else {
			if (chosen.y > robber.y) {
				--chosen.y;
			} else if (chosen.y < robber.y) {
				++chosen.y;
			}
		}
	}
	
	public static void moveRobber(int n) {
		if (robber.x == cop.x) {
			if (cop.x > (n/2-1)) {
				robber.x--;
			} else {
				robber.x++;
			}
		} else {
			if (cop.y > (n/2-1)) {
				robber.y--;
			} else {
				robber.y++;
			}
		}
	}
	
	public static void moveRobberRandom() {
		if (terning.nextInt(2) == 0) {
			if (terning.nextInt(2) == 0) {
				robber.translate(1, 0);
			} else {
				robber.translate(-1, 0);
			}
		} else {
			if (terning.nextInt(2) == 0) {
				robber.translate(0, 1);
			} else {
				robber.translate(0, -1);
			}
		}
	}
}
