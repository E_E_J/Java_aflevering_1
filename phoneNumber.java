/*
 * I denne opgave skal koden teste om det input den f�r er et dansk telefonnummer.
 * De specifikke krav til dette st�r i opgavebeskrivelsen, som der refereres til, n�r p�g�ldende krav testes.
 */

import java.util.*;															// Importeres for brug af scanneren

public class phoneNumber {				
	public static void main(String[] args) {								//Main kalder metoden 'testNumber' og printer det, metoden sender retur
		Scanner console = new Scanner(System.in);
		String number = console.nextLine();
		System.out.println(testNumber(number));
	}
																			
	public static boolean testNumber(String input) {
		char x;																//En variabel af datatypen 'char' defineres
		int numCounter = 0;													//Variablen her defineres og tildeles en v�rdi. Det er t�lleren som t�ller antallet af tal, der er i det input der er givet
		char onlyNumber[] = new char[input.length()];						//En liste defineres med l�ngden af det givne input. Listen bruges til at gemme 'de rene tal'-
																			//	(Alts� uden andre karakterer og mellemrum).
		
		
		for (int i = 0; i < input.length(); ++i) {							//Et forloop bruges til at t�lle antallet af tal i input'et og gemme dem i listen
			x = input.charAt(i);											//T�lleren 'i' v�lger karakteren der gemmes for hver gang loopet k�rer
			if (Character.isDigit(x)) {										//Test om karakteren er et tal
				onlyNumber[numCounter] = x;									
				numCounter++;												//Tal-t�lleren bruges til at gemme tallene i listen og t�lleren t�lles op
			}
		}
		
		
		
/*
 * Denne if-statement er sv�r at l�se, men fjerner redundans ifht. at have 3 if-statements, der alle skulle sende en falsk v�rdi retur. Den kan opdeles i 3 tests hvor der st�r 'eller'
 * imellem. De to 'eller' tegn der st�r som hovedkonjektiver er markeret med flere mellemrum p� hver side.
 * 
 * Hvis en af de 3 tests der laves giver true, returnerer den falsk.
 * 		- Den f�rste test tjekker, at der er 10 tal efter et '+' karakter, da der kommer to mere i form af '45'.
 * 		- Den anden test ser p�, at der er st�r 45 f�rst i tallet og det ikke er efterfulgt af 0, hvis det er et 10-cifret nummer.
 * 		- Den tredje test s�rger for, at der i et 8-cifret nummer ikke st�r 0 f�rst.
 */

		if ((input.charAt(0) == '+' && numCounter != 10)    ||    (numCounter == 10 && ((onlyNumber[0] != '4') ||					
				(onlyNumber[1] != '5') || (onlyNumber[2] == '0')))    ||    (numCounter == 8 && onlyNumber[0] == '0')) {					
			return false;
			
		} else if (numCounter == 10 || numCounter == 8){					//Hvis nummeret er 10- eller 8-cifret
			
			for (int i = 0; i < input.length(); ++i) {						//I dette forloop l�ber tallet igennem for at tjekke, at det kun best�r af tal og mellemrum.
				x = input.charAt(i);
				
				if (!Character.isDigit(x) && !Character.isWhitespace(x)) {	//Her er selve testen
					if (i == 0) {
						if (!(x == '+')) {									//Der en en undtagelse i form af et '+' tegn, hvis det st�r p� f�rste plads, det skal der tages h�jde for.
							return false;
						}
					} else {												//Hvis det er et ulovligt tegn returneres falsk
						return false;
					}
				}
			}
		} else {															//Hvis den kommer hertil er l�ngden forkert og falsk returneres
			return false;
		}
		return true;														//L�ber den igennem hele den test ovenfor uden at returnerer falsk, returneres der sandt, da kriterierne for et nummer er opfyldt
	}
}
